## Cinnamon SIG 

Cinnamon是一个易上手的，高度可定制的桌面环境，其特点深受Linux爱好者喜爱

## 工作目标

负责移植Cinnamon桌面环境，让其在openKylin拥有完美的体验



## SIG成员

* SIG负责人: DSOE1024


## 加入我们
如果您对移植桌面环境有兴趣，或者有相关打包经验，欢迎加入我们


## SIG维护包列表