# RV64G SIG

RV64G作为RISC-V一个子架构，支持RISC-V imafd指令集，相比RV64GC，不支持压缩指令集（c），本SIG负责RV64G开源软件包的维护。发布openKylin的RV64G 版本，进行软件包构建、系统构建等工作。

## 工作目标

- 负责openKylin RV64G版本的规划、制作、维护和升级
- 探索RISC-V各子架构应用兼容问题

## 邮件列表

rv64g@lists.openkylin.top

## SIG成员

### Owner

- [ianjiangict](https://gitee.com/ianjiangict)
- [cybergalaxy](https://gitee.com/cybergalaxy)

### Maintainers

- [fanxiaoqi@smart-core.cn](https://gitee.com/fan-xiaoqi12)
- [cybergalaxy](https://gitee.com/cybergalaxy)
- [ianjiangict](https://gitee.com/ianjiangict)

### Contributors

- [lundi001](https://gitee.com/lundi001)
- [jing2002](https://gitee.com/jing2002)

## 仓库列表
- [空]
