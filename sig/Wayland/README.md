# Wayland(SIG)
openKylin Wayland小组致力于新一代图形显示服务器相关技术研究，包括wayland合成器、X兼容等。提供wayland相关软件包的技术规划、设计、开发、维护和升级服务，共同推动新一代图形服务器技术发展及落地.

## 工作目标
- 自研openKylin wayland 合成器：实现基于wlroots kylin-wayland-compositor(简称wlcom)
- 负责openKylin wayland 图栈相关技术发展和决策
- 负责openKylin wayland 图栈相关软件包的规划、升级和维护
- 及时响应openKylin wayland 图栈相关用户反馈及问题解决

## SIG成员
### Owner
- [miczy](https://gitee.com/miczy)
- [JefferyGao](https://gitee.com/JefferyGao)
- [cryingpig](https://gitee.com/cryingpig)

### Maintainers
- [ylljhpqj](https://gitee.com/ylljhpqj)
- [kylin0061](https://gitee.com/kylin0061)
- [lianshanIn17](https://gitee.com/lianshanIn17)
- [lemon-smile886](https://gitee.com/lemon-smile886)
- [sunzhguy](https://gitee.com/sunzhguy)
- [clarleszhao2008](https://gitee.com/clarleszhao2008)
- [tiger_paper](https://gitee.com/tiger_paper)
- [apenper](https://gitee.com/apenper)
- [teangt](https://gitee.com/teangt)
- [amoskong@Sietium](https://gitee.com/amoskong)
- [fighting_liu](https://gitee.com/fighting-liu)
- [sosaxu](https://gitee.com/sosaxu)

### Contributors
- [sunlight-open](https://gitee.com/sunlight-open)
- [xia-123361](https://gitee.com/xia-123361)
- [yangcanfen](https://gitee.com/yangcanfen)
- [situation](https://gitee.com/situation)
- [lihongtao123456](https://gitee.com/lihongtao123456)
- [binzemin](https://gitee.com/binzemin)
- [rick511](https://gitee.com/rick511)
- [chenyongxing](https://gitee.com/chenyongxing)

### SIG维护包列表
- [wayland-protocols](https://gitee.com/openkylin/wayland-protocols)
- [wayland](https://gitee.com/openkylin/wayland)
- [wlroots](https://gitee.com/openkylin/wlroots)
- [xwayland](https://gitee.com/openkylin/xwayland)
- [kylin-wayland-compositor](https://gitee.com/openkylin/kylin-wayland-compositor)

### 邮件列表
wayland@lists.openkylin.top
