## 输入解决方案SIG组

### 工作目标

本SIG组致力于组建输入解决方案框架以及输入解决方案开源社区,推进输入解决方案框架以及输入解决方案在社区维护。输入解决方案，提供包括面板、引擎等输入解决方案所需要的组件支持。

### SIG成员
- jiangjiang@taotics-inc.com
- linmobei@taotics-inc.com
- tianyuan@taotics-inc.com
- shiyi@taotics-inc.com

### SIG维护包列表
