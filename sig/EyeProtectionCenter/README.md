# EyeProtectionCenter兴趣小组（SIG）

 EyeProtectionCenter SIG致力于维护护眼中心相关代码。

## 工作目标

- 维护护眼中心相关代码，并积极根据用户反馈解决相关问题。

## 维护包列表

- eyeprotection-backend
- eye-protection-center 

## SIG 成员

### Owner
- 宁思光(ningsiguang@kylinos.cn)
### Maintainers
- 王伟男(wangweinan@kylinos.cn)
- 焦点(jiaodian@kylinos.cn)

## 邮件列表

