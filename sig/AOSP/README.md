# AOSP兴趣小组（SIG）

AOSP SIG组负责AOSP在云手机和安卓硬件生态设备上的适配开发、版本维护、源码仓库管理和开发手册编写等工作，致力于打造基于AOSP的开源智能终端操作系统。


## 工作目标

- 提供在瑞芯微、展锐、Pixel系列等硬件平台上的内核，支撑AOSP运行;
- 完成Android 12等AOSP版本在瑞芯微、展锐、Pixel系列等硬件平台上的新特性开发；
- 实现安卓系统裁剪、定制改造和性能优化，提供安卓系统解决方案；
- 适配新的安卓硬件生态设备元器件，实现对安卓HAL层的扩展;
- 基于Cuttlefish虚拟化套件，在X86和ARM架构上虚拟出高保真的Android设备;
- 实现麒麟移动运行环境（KMRE），支持安卓应用在Linux上高效稳定运行。


## SIG成员

### Owner

- Duo Zhang(zhangduo@kylinos.cn)
- [balloonflower](https://gitee.com/balloonflower)

### Maintainers

- [balloonflower](https://gitee.com/balloonflower)
- [kmre-clom](https://gitee.com/kmre-clom)
- [mccaaab](https://gitee.com/mccaaab)
- [binghaiwanglifang](https://gitee.com/binghaiwanglifang)
- [zhuisu930](https://gitee.com/zhuisu930)
- [xsjkiver](https://gitee.com/xsjkiver)
- [Spirituality_Tao](https://gitee.com/Spirituality_Tao)
- [ikunXY](https://gitee.com/ikunXY)
- [youyuan_2022](https://gitee.com/youyuan_2022)
- [lzhGitwp](https://gitee.com/lzhGitwp)
- [kylin-huanglei](https://gitee.com/kylin-huanglei)
- [winter91](https://gitee.com/winter91)
- [linkgong](https://gitee.com/linkgong)
- [threewater000](https://gitee.com/threewater000)
- [jason.R.xie](https://gitee.com/jason.R.xie)
- [agor](https://gitee.com/agor)


## SIG维护包列表

- platform_manifest
- platform_build
- platform_build_soong
- platform_frameworks_base
- platform_frameworks_native
- platform_system_core
- platform_system_sepolicy
- platform_packages_apps_launcher3
- platform_system_connectivity_wificond
- platform_packages_modules_wifi
- platform_packages_modules_connectivity
- device_google_redfin
- platform_kernel-5.15
- device_google_cuttlefish
- platform_external_crosvm
- device_generic_vulkan-cereal
- kmre
- kylin-kmre-manager
- kylin-kmre-window
- kylin-kmre-daemon
- kylin-kmre-emugl
- kylin-kmre-display-control
- kylin-kmre-image-date
- kylin-kmre-image-update
- libkylin-kmre
- peony-vfs-kylin-kmre
- kmre_gps
- kmre_sensor
- kmre_device_audio
- kmre_apps_kmrelauncher
- kmre_apps_kmremanager
- device_generic_kmre
- kmre_apps_inputmethod
- platform_external_display_control


## SIG邮件列表

aosp@lists.openkylin.top
